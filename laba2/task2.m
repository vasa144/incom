x=infsup(-1.5,4)
y=infsup(-3,4)
z = globopt0([x,y])

for i = 1:1000
    xl = z(i).Box(1).inf
    xr = z(i).Box(1).sup
    yl = z(i).Box(2).inf
    yr = z(i).Box(2).sup
    rectangle('Position',[xl yl xr-xl yr-yl])
end
axis([-3 5 -4 5])


