x=infsup(-5,5)
y=infsup(-5,5)
z = globopt0([x,y])

for i = 1:200
    xl = z(i).Box(1).inf
    xr = z(i).Box(1).sup
    yl = z(i).Box(2).inf
    yr = z(i).Box(2).sup
    rectangle('Position',[xl yl xr-xl yr-yl])
end
axis([-8 8 -8 8])

for i = 1:200
    disp(sprintf('[%0.3f %0.3f],[%0.3f %0.3f] & %0.3f \\',z(i).Box(1).inf,z(i).Box(1).sup , z(i).Box(2).inf , z(i).Box(2).sup, z(i).Estim))
end
