% A=[infsup(9,13) infsup(13,17);infsup(15,19) infsup(15,19);infsup(19,23) infsup(9,13)]
% b=[infsup(2.1,6.1);infsup(2.6,7.6);infsup(1.8,6.8)]
% [V P1 P2 P3 P4]=EqnTolR2(inf(A),sup(A),inf(b),sup(b))
% rectangle("Position",[-0.08 0.08 0.28 0.28 ])
% rectangle("Position",[0.06 0.23 0.003 0.003 ],"EdgeColor","r")
% text(0.06+0.02,0.23,"argmaxTol","FontSize",8)

A=[infsup(9,13) infsup(15,19) infsup(19,23); infsup(13,17) infsup(15,19) infsup(9,13)]
b=[infsup(8.8,12.8);infsup(5.7,10.7)]
[Z]=EqnTolR3(inf(A),sup(A),inf(b),sup(b),1,1)

