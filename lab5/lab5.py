import numpy as np
from matplotlib import pyplot as plt
%matplotlib inline
import seaborn as sns
from kaucherpy import *

def sti(inf, sup):
    return np.append(-inf, sup)

def sti_inv(sti):  
    mid_idx = sti.shape[0] // 2
    return -sti[:mid_idx], sti[mid_idx:]

def sti_mul_mtx(matrix):
    pos = matrix.copy()
    neg = matrix.copy()
    pos[pos < 0] = 0
    neg[neg > 0] = 0
    neg = np.fabs(neg)
    return np.block([[pos, neg], [neg, pos]])

def make_interval_A(mtx):
    n = mtx.shape[0]
    rads = np.random.uniform(low=1e-8, high=0.001, size=(n, n))
    mtx_inf = mtx - rads
    mtx_sup = mtx + rads
    return mtx_inf, mtx_sup

def generate_right_part(matrix):
    n = matrix.shape[0]
    x = np.random.uniform(low=1, high=5, size=n)
    b = np.dot(matrix, x)  
    rads = np.random.uniform(low=0.5, high=2, size=n) 
    b_inf = b - rads 
    b_sup = b + rads
    return b_inf, b_sup, x

def find_matrix(matrix, size):
    M = matrix
    indexes = []

    for i, col in enumerate(M.T):
        if np.fabs(col).sum() > 0:
            indexes.append(i)
    M = M.T[indexes].T

    size = min(M.shape[0], M.shape[1], size)
    
    while size > 0:
        s = 0
        for i in range(M.shape[0] - size + 1):
            s = 0
            for j in range(M.shape[1] - size + 1):
                s = 0
                if np.linalg.matrix_rank(M[i:i+size, j:j+size]) == size:
                    print(j)
                    break

                s+=1

            if s == 0:
                print(i)
                break


        if s == 0:
            print(size)
            break

        size -=1
    
    return M[i:i+size, j:j+size]

def sub_grad(prev_D, A, n, x):
    mid = prev_D.shape[0] // 2
    for i in range(n):
        for j in range(n):
            val_low  = A[i][j].lower
            val_high = A[i][j].upper
            b_low = -x[j]
            b_high = x[j + n]
            if val_low * val_high > 0:
                k = 0 if val_low > 0 else 2
            else:
                k = 1 if val_low < val_high else 3

            if b_low * b_high > 0:
                m = 1 if b_low > 0 else 3
            else:
                m = 2 if b_low <= b_high else 4

            case = 4 * k + m
            if case == 1:
                prev_D[i, j] = val_low
                prev_D[i + mid, j + mid] = val_high
            elif case == 2:
                prev_D[i, j] = val_high
                prev_D[i + mid, j + mid] = val_high
            elif case == 3:
                prev_D[i, j] = val_high
                prev_D[i + mid, j + mid] = val_low
            elif case == 4:
                prev_D[i, j] = val_low
                prev_D[i + mid, j + mid] = val_low
            elif case == 5:
                prev_D[i, j + mid] = val_low
                prev_D[i + mid, j + mid] = val_high
            elif case == 6:
                if val_low * b_high < val_high * b_low:
                    prev_D[i, j + mid] = val_low
                else:
                    prev_D[i, j] = val_high
                if val_low * b_low > val_high * b_high:
                    prev_D[i + mid, j] = val_low
                else:
                    prev_D[i + mid, j + mid] = val_high
            elif case == 7:
                prev_D[i, j] = val_high
                prev_D[i + mid, j] = val_low
            elif case == 8:
                pass
            elif case == 9:
                prev_D[i, j + mid] = val_low
                prev_D[i + mid, j] = val_high
            elif case == 10:
                prev_D[i, j + mid] = val_low
                prev_D[i + mid, j] = val_low
            elif case == 11:
                prev_D[i, j + mid] = val_high
                prev_D[i + mid, j] = val_low
            elif case == 12:
                prev_D[i, j + mid] = val_high
                prev_D[i + mid, j] = val_high
            elif case == 13:
                prev_D[i, j] = val_low
                prev_D[i + mid, j] = val_high
            elif case == 14:
                pass
            elif case == 15:
                prev_D[i, j + mid] = val_high
                prev_D[i + mid, j + mid] = val_low
            elif case == 16:
                if val_low * b_low > val_high * b_high:
                    prev_D[i, j] = val_low
                else:
                    prev_D[i, j + mid] = -val_high
                if val_low * b_high < val_high * b_low:
                    prev_D[i + mid, j + mid] = val_low
                else:
                    prev_D[i + mid, j] = val_high
    return prev_D


def solve(A_inf, A_sup , b_inf, b_sup, eps=1e-5):
    n = A_inf.shape[0]
    print(n)
    A_int = [[Kaucher(A_inf[i, j], A_sup[i, j]) for j in range(n)] for i in range(n)]

    alpha = 0.8
    sti_b = sti(b_inf, b_sup)
    x = np.zeros_like(sti_b)
    prev = x
    iterations = 0
    worklist = [x]
    while (not iterations or np.linalg.norm(x - prev) > eps) and iterations < 100:
        iterations += 1
        prev = x
        subgrad = np.zeros((2 * n, 2 * n))
        subgrad = sub_grad(subgrad, A_int, n, prev)
        inf, sup = sti_inv(x)
        y_interval = [Kaucher(inf[i], sup[i]) for i in range(inf.shape[0])]
        prod = [sum([A_int[i][j] * y_interval[j] for j in range(len(y_interval))]) for i in range(len(A_int))]
        low = np.array([comp.lower for comp in prod])
        up = np.array([comp.upper for comp in prod])

        func_v = sti(low, up) - sti_b
        dx = np.linalg.solve(subgrad, -func_v)
        x = prev + alpha * dx
        worklist.append(x)
    return sti_inv(x), iterations, worklist


A2 = np.loadtxt('matrix_n_phi_1.txt')
A2 = find_matrix(A2,size = 14)
b_inf, b_sup, x = generate_right_part(A2)
A_inf, A_sup = make_interval_A(A2)

res , iter, worklist = solve(A_inf, A_sup, b_inf, b_sup)
print(np.array(res).T)
plt.figure(figsize=(20,10))
sns.heatmap(A2)
plt.show()
plt.figure(figsize=(20,10))
plt.figure(figsize=(20,10))
plt.plot(x, label = "x")
plt.plot(res[0],label="inf")
plt.plot(res[1],label="sup")
plt.legend()
plt.grid()
plt.show()
plt.figure(figsize=(20,10))
norms = [np.linalg.norm(worklist[i + 1] - worklist[i]) for i in range(len(worklist) - 1)]
plt.plot(norms)
plt.xlabel('i')
plt.ylabel(r"$||x^{(i + 1)} - x^{(i)}||$")
plt.legend()
plt.grid()
plt.show()


A3 = np.loadtxt('matrix_n_phi_6.txt')
A3 =  find_matrix(A3,size = 20)
b_inf3, b_sup3, x = generate_right_part(A3)
A_inf3, A_sup3 = make_interval_A(A3)

res3 , iter3, worklist3 = solve(A_inf3, A_sup3, b_inf3, b_sup3)

print(np.array(res3).T)
plt.figure(figsize=(20,10))
sns.heatmap(A3)
plt.show()
plt.figure(figsize=(20,10))
plt.plot(x3, label = "x")
plt.plot(res3[0],label="inf")
plt.plot(res3[1],label="sup")
plt.legend()
plt.grid()
plt.show()
plt.figure(figsize=(20,10))
norms3 = [np.linalg.norm(worklist3[i + 1] - worklist3[i]) for i in range(len(worklist3) - 1)]
plt.plot(norms3)
plt.xlabel('i')
plt.ylabel(r"$||x^{(i + 1)} - x^{(i)}||$")
plt.legend()
plt.grid()
plt.show()