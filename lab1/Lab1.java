

public class Lab1 {


    boolean task2Invertible(int size, double eps) {
        Interval diag = new IntervalKauher(1, 1);
        Interval nonDiag = new IntervalKauher(0, eps);
        Interval det = new Matrix(Matrix.MatrixE(size, nonDiag, diag)).determinant();
        double down = ((DigitDouble) ((IntervalKauher) det).getLowerBound()).value;
        double up = ((DigitDouble) ((IntervalKauher) det).getUpperBound()).value;
        if (up > down) {
            return up >= 0 && down <= 0;
        } else {
            return up <= 0 && down >= 0;
        }
    }


    boolean task1Invertible(double eps) {
        Interval[][] mtx = new Interval[2][2];
        mtx[0][0] = new IntervalKauher(1 - eps, 1 + eps);
        mtx[1][0] = new IntervalKauher(1.1 - eps, 1.1 + eps);
        mtx[0][1] = mtx[0][0];
        mtx[1][1] = mtx[0][0];
        Interval det = new Matrix(mtx).determinant();
        double down = ((DigitDouble) ((IntervalKauher) det).getLowerBound()).value;
        double up = ((DigitDouble) ((IntervalKauher) det).getUpperBound()).value;
        if (up > down) {
            return up >= 0 && down <= 0;
        } else {
            return up <= 0 && down >= 0;
        }
    }

    double task1 (double delta){
        for (double i = 0; i <=0.05 ; i+=delta) {
            if(task1Invertible(i)){
                return i;
            }
        }
        return 1;
    }

    double task2 (double delta, int size){
        for (double i = 0; i <=1 ; i+=delta) {
            if(task2Invertible(size,i)){
                return i;
            }
        }
        return 2;
    }

    void task1Start(){
        System.out.println("task1");
        double [] delta = new double[]{0.01,0.001,0.0001,0.00001,0.000001};
        for (int i = 0; i < delta.length; i++) {
            System.out.println(delta[i] + " & "+ task1(delta[i]) + "\\"+"\\");
            System.out.println("\\hline");
        }
    }

    void task2Start(){
        System.out.println("task2");
        double [] delta = new double[]{0.01,0.001,0.0001,0.00001,0.000001};
        int [] size = new int[]{3,4,5,6};
        for (int j = 0; j < size.length; j++) {
            for (int i = 0; i < delta.length; i++){
                    System.out.println(size[j] + " & " + delta[i] + " & "+ task2(delta[i], size[j])+ "\\"+"\\");
                System.out.println("\\hline");
            }
        }
    }

    public static void main(String[] args) {
        Lab1 lab1 = new Lab1();
       //lab1.task1Start();
       lab1.task2Start();
    }
}
